﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Codster.Api.Models
{
    public partial class MyDbContext : DbContext
    {
        public MyDbContext()
        {
        }

        public MyDbContext(DbContextOptions<MyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Allergies> Allergies { get; set; }
        public virtual DbSet<Casefiles> Casefiles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("Server=localhost;User Id=root;Password=dev123!;Database=codster");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Allergies>(entity =>
            {
                entity.HasKey(e => e.IdAllergies);

                entity.ToTable("allergies");

                entity.HasIndex(e => e.CaseFileId)
                    .HasName("CaseFiles_idx");

                entity.Property(e => e.IdAllergies)
                    .HasColumnName("idAllergies")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CaseFileId).HasColumnType("int(11)");

                entity.Property(e => e.DischargeDate)
                    .HasColumnName("Discharge Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Medicine)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.HasOne(d => d.CaseFile)
                    .WithMany(p => p.Allergies)
                    .HasForeignKey(d => d.CaseFileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CaseFiles");
            });

            modelBuilder.Entity<Casefiles>(entity =>
            {
                entity.HasKey(e => e.IdCaseFile);

                entity.ToTable("casefiles");

                entity.Property(e => e.IdCaseFile)
                    .HasColumnName("idCaseFile")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BloodType)
                    .IsRequired()
                    .HasColumnType("varchar(4)");

                entity.Property(e => e.CaseFileNumber)
                    .IsRequired()
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.DateLastQuery).HasColumnType("datetime");
            });
        }
    }
}
