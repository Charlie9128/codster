﻿using System;

namespace Codster.Api.Models
{
    public partial class Allergies
    {
        public int IdAllergies { get; set; }
        public string Name { get; set; }
        public DateTime DischargeDate { get; set; }
        public string Medicine { get; set; }
        public int CaseFileId { get; set; }
        public Casefiles CaseFile { get; set; }
    }
}
