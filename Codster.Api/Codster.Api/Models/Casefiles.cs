﻿using System;
using System.Collections.Generic;

namespace Codster.Api.Models
{
    public partial class Casefiles
    {
        public Casefiles()
        {
            Allergies = new HashSet<Allergies>();
        }

        public int IdCaseFile { get; set; }
        public int UserId { get; set; }
        public string CaseFileNumber { get; set; }
        public DateTime DateLastQuery { get; set; }
        public string BloodType { get; set; }
        public IEnumerable<Allergies> Allergies { get; set; }
    }
}
