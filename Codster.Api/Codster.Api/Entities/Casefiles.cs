﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Codster.Api
{
    public partial class Casefiles
    {
        public Casefiles()
        {
            Allergies = new HashSet<Allergies>();
        }

        public int IdCaseFile { get; set; }
        public int UserId { get; set; }
        public string CaseFileNumber { get; set; }
        public DateTime DateLastQuery { get; set; }
        public string BloodType { get; set; }
        public ICollection<Allergies> Allergies { get; set; }
    }
}
