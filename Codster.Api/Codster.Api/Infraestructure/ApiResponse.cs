﻿using Newtonsoft.Json;

namespace Codster.Api.Infraestructure
{
    public class ApiResponse
    {
        public int StatusCode { get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; }

        public ApiResponse(int statusCode, string message = null)
        {
            StatusCode = statusCode;
            Message = message ?? GetDefaultMessageForStatusCode(statusCode);
        }

        private static string GetDefaultMessageForStatusCode(int statusCode)
        {
            switch (statusCode)
            {
                case 200:
                    return "Petición Completada";
                case 400:
                    return "El id de usuario no existe.";
                case 404:
                    return "No se encontro el recurso.";
                case 500:
                    return "Ocurrió un error.";
                default:
                    return null;
            }
        }
    }
}
