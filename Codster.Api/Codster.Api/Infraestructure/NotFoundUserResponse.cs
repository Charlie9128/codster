﻿namespace Codster.Api.Infraestructure
{
    public class NotFoundUserResponse : ApiResponse
    {
        public NotFoundUserResponse() : base(400)
        {

        }
    }
}
