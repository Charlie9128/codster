﻿using System.Collections.Generic;
using Codster.Api.Infraestructure;
using Codster.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Codster.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        private readonly CaseFileService _service = new CaseFileService();

        [HttpGet]
        public IEnumerable<Models.Casefiles> Get()
        {
            return _service.GetCaseFiles();
        }

        [HttpGet]
        [Route("v1/accounts/{id}/record")]
        public IActionResult GetCaseFile(int id)
        {
            Models.Casefiles caseFile = _service.GetCaseFile(id);

            if (caseFile == null)
            {
                return NotFound(new NotFoundUserResponse());
            }

            return Ok(new ApiOkResponse(caseFile));
        }
    }
}
