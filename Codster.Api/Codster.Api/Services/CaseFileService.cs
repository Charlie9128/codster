﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity.UI.Pages.Internal.Account;

namespace Codster.Api.Services
{
    public class CaseFileService
    {
        private readonly MyDbContext _context = new MyDbContext();

        public IEnumerable<Models.Casefiles> GetCaseFiles()
        {
            return (from c in _context.Casefiles
                    join a in _context.Allergies on c.IdCaseFile equals a.CaseFileId
                    select new Models.Casefiles
                    {
                        IdCaseFile = c.IdCaseFile,
                        BloodType = c.BloodType,
                        CaseFileNumber = c.CaseFileNumber,
                        DateLastQuery = c.DateLastQuery,
                        Allergies = _context.Allergies.Where(x => x.CaseFileId == a.CaseFileId).Select(x => new Models.Allergies
                        {
                            IdAllergies = x.IdAllergies,
                            DischargeDate = x.DischargeDate,
                            Medicine = x.Medicine,
                            Name = x.Name,
                            CaseFileId = x.CaseFileId
                        })
                    });
        }

        public Models.Casefiles GetCaseFile(int userId)
        {
            return (from c in _context.Casefiles
                    join a in _context.Allergies on c.IdCaseFile equals a.CaseFileId
                    where c.UserId == userId
                    select new Models.Casefiles
                    {
                        IdCaseFile = c.IdCaseFile,
                        BloodType = c.BloodType,
                        CaseFileNumber = c.CaseFileNumber,
                        DateLastQuery = c.DateLastQuery,
                        Allergies = _context.Allergies.Where(x => x.CaseFileId == a.CaseFileId).Select(x => new Models.Allergies
                        {
                            IdAllergies = x.IdAllergies,
                            DischargeDate = x.DischargeDate,
                            Medicine = x.Medicine,
                            Name = x.Name,
                            CaseFileId = x.CaseFileId
                        })
                    }).FirstOrDefault();
        }
    }
}
