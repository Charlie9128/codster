CREATE TABLE IF NOT EXISTS `Codster`.`CaseFiles` (
  `idCaseFile` INT NOT NULL AUTO_INCREMENT,
  `UserId` INT NOT NULL,
  `CaseFileNumber` VARCHAR(10) NOT NULL,
  `DateLastQuery` DATETIME NOT NULL,
  `BloodType` VARCHAR(4) NOT NULL,
  PRIMARY KEY (`idCaseFile`))
ENGINE = InnoDB
