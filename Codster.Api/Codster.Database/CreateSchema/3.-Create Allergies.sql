CREATE TABLE IF NOT EXISTS `Codster`.`Allergies` (
  `idAllergies` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Discharge Date` DATETIME NOT NULL,
  `Medicine` VARCHAR(45) NOT NULL,
  `CaseFileId` INT NOT NULL,
  PRIMARY KEY (`idAllergies`),
  INDEX `CaseFiles_idx` (`CaseFileId` ASC) VISIBLE,
  CONSTRAINT `CaseFiles`
    FOREIGN KEY (`CaseFileId`)
    REFERENCES `Codster`.`CaseFiles` (`idCaseFile`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB